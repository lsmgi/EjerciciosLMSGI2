<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="callejero"> 
        <html>
            <head></head>
            <body>
                <!-- ///B/// -->
                <h1>
                    <xsl:value-of select="@barrio"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="@ciudad"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="@cod_postal"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="@provincia"/>
                </h1>
                <img>
                    <xsl:attribute name="src"><xsl:value-of select="imagen"/></xsl:attribute>
                </img>
                <!-- ///B/// -->
                <br/>
                <br/>
                <!-- ///C/// -->
                <table border="1">
                    <xsl:apply-templates select="monumentos"></xsl:apply-templates> <!-- Uso de apply templates -->
                </table>
                <!-- ///C/// -->
                <!-- ///D/// -->
                <table border="1" width="90%">
                    
                    <xsl:attribute name="style">background: url(./images/callejero.png) no-repeat;</xsl:attribute>
                    
                    <xsl:call-template name="bucleForFila">
                        <xsl:with-param name="i">1</xsl:with-param> <!-- Valor inicial para la i -->
                    </xsl:call-template>
                    
                </table>
                <!-- ///D/// -->
            </body>
        </html>
    </xsl:template> 
   
   <xsl:template match="monumentos">
       
       <xsl:for-each select="monumento">
           <tr>
               <xsl:choose> <!-- Tres colores alternos -->
                   <xsl:when test="position() mod 3 = 1"><xsl:attribute name="style">background-color: cadetblue;</xsl:attribute></xsl:when>
                   <xsl:when test="position() mod 3 = 2"><xsl:attribute name="style">background-color: rgb(95, 160, 98);</xsl:attribute></xsl:when>
                   <xsl:otherwise><xsl:attribute name="style">background-color: rgb(235, 226, 99);</xsl:attribute></xsl:otherwise>
               </xsl:choose> <!-- Tres colores alternos -->
               <td>
                   <xsl:call-template name="imagen"> <!-- Los monumentos se muestran -->
                       <xsl:with-param name="nomu"><xsl:value-of select="@imagen"/></xsl:with-param>
                   </xsl:call-template>
               </td>
               <td><xsl:value-of select="@nombre"/></td>
               <td><xsl:value-of select="@calle"/></td>
           </tr>
       </xsl:for-each>
       
   </xsl:template>
    
   <xsl:template name="imagen">
       <xsl:param name="nomu"/>
           
       <img src="{/callejero/Imagenes/imagen[@id=$nomu]}"></img>
   </xsl:template>
   
    <!-- //////////////////////////////////BUCLE FOR/////////////////////////////// -->
    <xsl:template name="bucleForFila">
        <xsl:param name="i"/>
        <xsl:if test="$i &lt;= 6"> <!-- Asta que la i sea x -->
            <tr>
                <xsl:call-template name="bucleForColumna">
                    <xsl:with-param name="i"><xsl:value-of select="$i"/></xsl:with-param>
                    <xsl:with-param name="j">1</xsl:with-param> <!-- Valor inicial para l j -->
                </xsl:call-template>
            </tr>
            <xsl:call-template name="bucleForFila">
                <xsl:with-param name="i"><xsl:value-of select="$i + 1"/></xsl:with-param> <!-- por cada vuelta sumo x -->
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="bucleForColumna">
        <xsl:param name="i"/>
        <xsl:param name="j"/> 
        <xsl:if test="$j &lt;= 6"> <!-- Asta que la j sea x -->
            <xsl:call-template name="celda">
                <xsl:with-param name="x"><xsl:value-of select="$j"/></xsl:with-param> <!-- la j tendra el valor de la cordenada x es decir la filas -->
                <xsl:with-param name="y"><xsl:value-of select="$i"/></xsl:with-param> <!-- la i tendra el valor de la cordenada y es decir las columnas -->
            </xsl:call-template>
            <xsl:call-template name="bucleForColumna">
                <xsl:with-param name="i"><xsl:value-of select="$i"/></xsl:with-param>
                <xsl:with-param name="j"><xsl:value-of select="$j + 1"/></xsl:with-param> <!-- por cada vuelta sumo x -->
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="celda">
        <xsl:param name="x"/>
        <xsl:param name="y"/>
        <!-- ///PARTE A EDITAR/// -->
        <td>
            <xsl:for-each select="loquesadentrodelbucle">
                <xsl:if test="@x = $x and @y = $y"> <!-- si las variables x e y coinciden con los atributos del elemento ago algo -->
                    <xsl:value-of select="@nombre"/>
                </xsl:if>
            </xsl:for-each>
        </td>
        <!-- ///PARTE A EDITAR/// -->
    </xsl:template>
    <!-- //////////////////////////////////BUCLE FOR/////////////////////////////// -->
    
</xsl:stylesheet>