<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="ligaVoleibol">
        <html>
            <head>
                <title> <!-- //B// -->
                <xsl:value-of select="@pais"/> -
                <xsl:value-of select="@nombreLiga"/>
                </title> <!-- //B// -->
                <style>
                    
                    th {
                    background-color: blue;
                    color: white;
                    }
                    
                    teble {
                    margin: auto;
                    
                    }
                    
                </style>
            </head>
            <body>
                <h1>
                    <!-- //B// -->
                    <xsl:value-of select="@pais"/> - <xsl:value-of select="@nombreLiga"/>
                </h1><!-- //B// -->
                
                <h3>
                    Clasificacion
                </h3>

                <table border="1">
                    <tr>
                        <th>Imagen</th>
                        <th>Nombre equipo</th>
                        <th>Puntos</th>
                        <th>PArtidos Jugados</th>
                        <th>Ganados</th>
                        <th>Perdidos</th>
                        <th>Ganados contra perdidos</th>
                    </tr>
                    <xsl:apply-templates select="clasificacion"/>
                    <!-- Uso de apply templates -->
                </table>

            </body>
        </html>
    </xsl:template>
    
    <!-- ////////////////////////////////////////////////// -->
    <xsl:template match="clasificacion">
        <xsl:for-each select="equipo">
            
            <xsl:sort select="nombre"/>
            
            <tr>
                <xsl:call-template name="color">
                    <xsl:with-param name="posicion"><xsl:value-of select="position()"/></xsl:with-param>
                </xsl:call-template>
                <td>
                    <xsl:call-template name="imagen">
                        <xsl:with-param name="image"><xsl:value-of select="@id"/></xsl:with-param>
                    </xsl:call-template>
                </td>
                <td>
                    <xsl:value-of select="nombre"/>
                </td>
                <td>
                    <xsl:value-of select="@puntos"/>
                </td>
                <td>
                    <xsl:value-of select="@Jugados"/>
                </td>
                <td>
                    <xsl:value-of select="@ganados"/>
                </td>
                <td>
                    <xsl:value-of select="@perdidos"/>
                </td>
                <td>
                    <xsl:value-of select="@ganados - @perdidos"/>
                </td>
            </tr>
        </xsl:for-each>
        
    </xsl:template>
    
    <xsl:template name="imagen">
        <xsl:param name="image"/>
        <img src="{/ligaVoleibol/recursos/imagenes/img[$image=@id]}" width="50px" />
    </xsl:template>
    
    <xsl:template name="color">
        <xsl:param name="posicion"/>
        
        <xsl:choose> <!-- Tres colores alternos -->
            <xsl:when test="$posicion mod 3 = 1"><xsl:attribute name="style">background-color: lightblue;</xsl:attribute></xsl:when>
            <xsl:when test="$posicion mod 3 = 2"><xsl:attribute name="style">background-color: lightgoldenrodyellow;</xsl:attribute></xsl:when>
            <xsl:otherwise><xsl:attribute name="style">background-color: white;</xsl:attribute></xsl:otherwise>
        </xsl:choose> <!-- Tres colores alternos -->
    </xsl:template>
    
    
    
</xsl:stylesheet>