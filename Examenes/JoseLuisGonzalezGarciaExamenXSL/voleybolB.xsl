<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="ligaVoleibol">
        <html>
            <head>
                <title> <!-- //B// -->
                <xsl:value-of select="@pais"/> -
                <xsl:value-of select="@nombreLiga"/>
                </title> <!-- //B// -->
                <style>
                    
                    th {
                    background-color: blue;
                    color: white;
                    }
                    
                    table {
                    margin: auto;
                    background-image: url('./imagenes/mapaEspana.png');
                    background-size: cover;
                    }
                    
                    td {
                    width: 85px;
                    height: 60px;
                    }
                    
                    

                </style>
            </head>
            <body>
                
                <xsl:apply-templates select="mapa"/>
                    
            </body>
        </html>
    </xsl:template>
    
    <!-- ////////////////////////////////////////////////// -->
    
    <xsl:template match="mapa">
        <table border="1" with="90%">
            
            <xsl:call-template name="bucleForFila">
                <xsl:with-param name="i">1</xsl:with-param> <!-- Valor inicial para la i -->
            </xsl:call-template>
            
        </table>
    </xsl:template>
    

    <!-- //////////////////////////////////BUCLE FOR/////////////////////////////// -->
    <xsl:template name="bucleForFila">
        <xsl:param name="i"/>
        <xsl:if test="$i &lt;= 10"> <!-- Asta que la i sea x -->
            <tr>
                <xsl:call-template name="bucleForColumna">
                    <xsl:with-param name="i"><xsl:value-of select="$i"/></xsl:with-param>
                    <xsl:with-param name="j">1</xsl:with-param> <!-- Valor inicial para l j -->
                </xsl:call-template>
            </tr>
            <xsl:call-template name="bucleForFila">
                <xsl:with-param name="i"><xsl:value-of select="$i + 1"/></xsl:with-param> <!-- por cada vuelta sumo x -->
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="bucleForColumna">
        <xsl:param name="i"/>
        <xsl:param name="j"/> 
        <xsl:if test="$j &lt;= 10"> <!-- Asta que la j sea x -->
            <xsl:call-template name="celda">
                <xsl:with-param name="x"><xsl:value-of select="$j"/></xsl:with-param> <!-- la j tendra el valor de la cordenada x es decir la filas -->
                <xsl:with-param name="y"><xsl:value-of select="$i"/></xsl:with-param> <!-- la i tendra el valor de la cordenada y es decir las columnas -->
            </xsl:call-template>
            <xsl:call-template name="bucleForColumna">
                <xsl:with-param name="i"><xsl:value-of select="$i"/></xsl:with-param>
                <xsl:with-param name="j"><xsl:value-of select="$j + 1"/></xsl:with-param> <!-- por cada vuelta sumo x -->
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="celda">
        <xsl:param name="x"/>
        <xsl:param name="y"/>
        <!-- ///PARTE A EDITAR/// -->
        <td>
            <xsl:for-each select="equipo">
                <xsl:if test="CoordenadaX/@valor = $x and coordenadaY/@valor = $y">
                    <xsl:choose>
                        <xsl:when test="CoordenadaX/@valor mod 2 = 0">
                            <xsl:attribute name="style">background-color: purple;</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="style">background-color: mediumslateblue;</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:call-template name="imagen">
                        <xsl:with-param name="ide"><xsl:value-of select="@id"/></xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>
        </td>
        <!-- ///PARTE A EDITAR/// -->
    </xsl:template>
    <!-- //////////////////////////////////BUCLE FOR/////////////////////////////// -->

    <xsl:template name="imagen">
        <xsl:param name="ide"/>
        <img src="{/ligaVoleibol/recursos/imagenes/img[$ide=@id]}" width="50px" />
        <xsl:for-each select="/ligaVoleibol/clasificacion/equipo">
            <xsl:if test="@id = $ide"><xsl:value-of select="nombre"/></xsl:if>
        </xsl:for-each>
    </xsl:template>


</xsl:stylesheet>